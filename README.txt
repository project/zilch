
ABOUT 0 Zilch
-----------

Zilch means zero. The zilch theme is provided for light weight theme. Theme places a focus on usability and readability. Large text. Readability is important, and no one likes small print. Simplicity is key of theme, featuring plenty of white space, as well as a lack of superfluous elements.
